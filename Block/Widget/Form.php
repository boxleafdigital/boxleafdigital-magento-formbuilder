<?php
namespace BoxLeafDigital\FormBuilder\Block\Widget;

use BoxLeafDigital\FormBuilder\Api\AnswersRepositoryInterface;
use BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface;
use BoxLeafDigital\FormBuilder\Api\FormRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\LayoutFactory;
use Magento\Widget\Block\BlockInterface;

class Form extends \Magento\Framework\View\Element\Template implements BlockInterface, IdentityInterface
{
    const CACHE_TAG = 'Form';

    protected $_template = "BoxLeafDigital_FormBuilder::form/form.phtml";

    /**
     * @var FormRepositoryInterface
     */
    private $_formRepository;
    /**
     * @var LayoutFactory
     */
    private $_layoutFactory;
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var \Magento\Framework\Registry
     */
    private $_registry;
    /**
     * @var AnswersRepositoryInterface
     */
    private $answersRepository;
    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;

    public function __construct(
        Template\Context $context,
        FormRepositoryInterface $formRepository,
        LayoutFactory $layoutFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        AnswersRepositoryInterface $answersRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        \Magento\Framework\Registry $registry,
        Http $request,
        UrlInterface $urlBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->_formRepository = $formRepository;
        $this->_urlBuilder = $urlBuilder;
        $this->_layoutFactory = $layoutFactory;
        $this->_request = $request;
        $this->_registry = $registry;
        $this->productRepository = $productRepository;
        $this->answersRepository = $answersRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
    }

    private function getId()
    {
        return $this->getData('choose_form');
    }

    public function isCartConfigure()
    {
        if ($this->_request->getFullActionName() == 'checkout_cart_configure') {
            return true;
        }

        return false;
    }

    public function isJson($string)
    {
        return ((is_string($string) &&
            (is_object(json_decode($string)) ||
                is_array(json_decode($string))))) ? true : false;
    }

    public function getValues()
    {
        $values = [];
        $fields = $this->getFields();
        if ($this->isCartConfigure()) {
            $quoteItemId = $this->_request->getParam('id');

            foreach ($fields as $field) {
                $filters = $this->searchCriteriaBuilderFactory->create();
                $filters->addFilter('quote_item_id', $quoteItemId);
                $filters->addFilter('question_Id', $field->getQuestionsId());
                $r = $this->answersRepository->getList($filters->create());

                if ($r->getTotalCount() > 0) {
                    $item  = $r->getItems();
                    $item  =end($item);

                    $val = $item->getResultValue();
                    if ($this->isJson($val)) {
                        $val =  json_decode($val, true);
                    }
                    $values[$field->getQuestionsId()] =$val;
                } else {
                    if ($field->getDefaultAnswer()) {
                        $values[$field->getQuestionsId()] = $field->getDefaultAnswer();
                    } else {
                        $values[$field->getQuestionsId()] = '';
                    }
                }
            }
        } else {
            foreach ($fields as $field) {
                if ($field->getDefaultAnswer()) {
                    $values[$field->getQuestionsId()] = $field->getDefaultAnswer();
                } else {
                    $values[$field->getQuestionsId()] = '';
                }
            }
        }

        return $values;
    }

    public function getForm()
    {
        $id = false;

        if ($this->isProduct() || $this->isCartConfigure()) {
            if ($this->isProduct()) {
                $product = $this->productRepository->getById($this->_request->getParam('id'));
            }
            if ($this->isCartConfigure()) {
                $product = $this->productRepository->getById($this->_request->getParam('product_id'));
            }
            $id = $product->getForm();
            if ($id == null) {
                $id = $product->getData('pquestionnaire');
            }
        } else {
            $id = $this->getId();
        }
        if ($id) {
            try {
                $form = $this->_formRepository->get($id);
                return $form;
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    public function getFields()
    {
        $form = $this->getForm();
        if ($form) {
            return $form->getFields();
        }
        return [];
    }

    public function getSubmitUrl()
    {
        return $this->_urlBuilder->getUrl('forms/form/submitpost');
    }

    /**
     * @param $field QuestionsInterface
     * @return string
     */
    public function render($field)
    {
        $values =  $this->getValues();
        $type = $field->getAnswerType();
        $html =  $this->_layoutFactory->create()
            ->createBlock('BoxLeafDigital\FormBuilder\Block\Form\Types\Types')->setField($field);
        switch ($type) {
            default:
            case 'textbox':
            case 'inpt':
                $html->setType('text');
                $html->setTemplate('BoxLeafDigital_FormBuilder::form/field/types/textbox.phtml');
                break;
            case 'password':
                $html->setType('password');
                $html->setTemplate('BoxLeafDigital_FormBuilder::form/field/types/textbox.phtml');
                break;
            case 'email':
                $html->setType('email');
                $html->setTemplate('BoxLeafDigital_FormBuilder::form/field/types/textbox.phtml');
                break;
            case 'textarea':
                $html->setTemplate('BoxLeafDigital_FormBuilder::form/field/types/textarea.phtml');
                break;
            case 'select':
            case 'slct':
                $html->setTemplate('BoxLeafDigital_FormBuilder::form/field/types/select.phtml');
                break;
            case 'radio':
                $html->setTemplate('BoxLeafDigital_FormBuilder::form/field/types/radio.phtml');
                break;
            case 'checkbox':
            case 'chck':
                $html->setTemplate('BoxLeafDigital_FormBuilder::form/field/types/checkox.phtml');
                break;

        }

        $html->setData('value', $values[$field->getQuestionsId()]);
        return $html->toHtml();
    }

    public function isProduct()
    {
        if ($this->_request->getFullActionName() == 'catalog_product_view') {
            return true;
        }

        return false;
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        $identities = [
            self::CACHE_TAG . '_' . $this->getId(),
        ];

        return $identities;
    }
}
