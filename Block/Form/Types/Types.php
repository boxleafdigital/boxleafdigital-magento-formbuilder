<?php
namespace BoxLeafDigital\FormBuilder\Block\Form\Types;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\LayoutFactory;

class Types extends \Magento\Framework\View\Element\Template
{
    /**
     * @var LayoutFactory
     */
    private $_layoutFactory;

    public function __construct(
        Template\Context $context,
        LayoutFactory $layoutFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->_layoutFactory = $layoutFactory;
    }


}
