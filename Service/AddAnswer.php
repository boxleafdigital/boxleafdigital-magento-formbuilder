<?php

namespace BoxLeafDigital\FormBuilder\Service;

use BoxLeafDigital\FormBuilder\Api\Data\AnswersInterfaceFactory;
use BoxLeafDigital\FormBuilder\Model\AnswersRepository;
use BoxLeafDigital\FormBuilder\Model\QuestionsRepository;
use Magento\Customer\Model\Session;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class AddAnswer
 * @package BoxLeafDigital\FormBuilder\Service
 */
class AddAnswer
{

    /**
     * @var AnswersInterfaceFactory
     */
    private $answersInterfaceFactory;
    /**
     * @var AnswersRepository
     */
    private $answersRepository;
    /**
     * @var Session
     */
    private $customerSession;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var SearchCriteriaBuilderFactory
     */
    private $searchCriteriaBuilderFactory;
    /**
     * @var QuestionsRepository
     */
    private $questionsRepository;

    /**
     * SubmitPost constructor.
     * @param AnswersInterfaceFactory $answersInterfaceFactory
     * @param AnswersRepository $answersRepository
     * @param QuestionsRepository $questionsRepository
     * @param Session $customerSession
     * @param SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        AnswersInterfaceFactory $answersInterfaceFactory,
        AnswersRepository $answersRepository,
        QuestionsRepository $questionsRepository,
        Session $customerSession,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        ManagerInterface $messageManager
    ) {
        $this->answersInterfaceFactory =$answersInterfaceFactory;
        $this->answersRepository =$answersRepository;
        $this->customerSession =$customerSession;
        $this->searchCriteriaBuilderFactory =$searchCriteriaBuilderFactory;
        $this->messageManager =$messageManager;
        $this->questionsRepository =$questionsRepository;
    }

    public function execute($data, $showMessage = true, $quoteId = null, $newQuoteId = null)
    {
        $results =[];
        if (isset($data['question'])) {
            $guest = $this->customerSession->isLoggedIn() ? false : true;
            $customerId = $guest == false ? $this->customerSession->getCustomerId() : null;

            $gtid = $this->customerSession->getSessionId();
            $unique = uniqid();
            $failed =false;

            foreach ($data['question'] as $id => $val) {
                $filters = $this->searchCriteriaBuilderFactory->create();
                $answer = $this->answersInterfaceFactory->create();

                if ($quoteId != null) {
                    $filters->addFilter('quote_item_id', $quoteId);

                    //$filters->addFilter('result_value', $val);
                    $filters->addFilter('question_Id', $id);
                    $r = $this->answersRepository->getList($filters->create());

                    if ($r->getTotalCount() > 0) {
                        foreach ($r->getItems() as $item) {
                            try {
                                $answer = $this->answersRepository->get($item->getAnswersId());
                                break;
                            } catch (LocalizedException $e) {
                            }
                        }
                    }
                }

                try {
                    $q = $this->questionsRepository->get($id);
                    $answer->setQuestionCode($q->getQuestioncode());
                } catch (\Exception $e) {
                }
                $answer->setQuestionId($id);
                $answer->setIsGuest($guest);
                $answer->setCustomerId($customerId == null ? $gtid : $customerId);
                $answer->setResultId($unique);
                $answer->setResultValue(is_array($val) ? json_encode($val) : $val);

                if ($quoteId) {
                    $answer->setQuoteItemId($quoteId);
                }

                if ($newQuoteId) {
                    $answer->setQuoteItemId($newQuoteId);
                }

                try {
                    $answer = $this->answersRepository->save($answer);
                    $results[] = $answer->getAnswersId();
                } catch (\Exception $e) {
                    $failed = true;
                }
            }

            if ($failed == true) {
                foreach ($results as $result) {
                    try {
                        $this->answersRepository->delete($result);
                    } catch (\Exception $e) {
                    }
                }

                if ($showMessage) {
                    $this->messageManager->addErrorMessage(__('Form failed to submit.'));
                    return false;
                }
            } else {
                if ($showMessage) {
                    $this->messageManager->addSuccessMessage(__('Form submitted Successfully'));
                }
            }
        }

        return $results;
    }
}
