<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model\Form;

use BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface;
use BoxLeafDigital\FormBuilder\Model\ResourceModel\Form\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $loadedData;
    protected $collection;

    protected $dataPersistor;
    private $questions;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \BoxLeafDigital\FormBuilder\Model\ResourceModel\Questions\CollectionFactory $questionCollection
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Questions\CollectionFactory $questionCollection,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->questions = $questionCollection->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
            $this->applyQuestions($model);
        }
        $data = $this->dataPersistor->get('boxleafdigital_formbuilder_form');

        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->applyQuestions($model);
            $this->dataPersistor->clear('boxleafdigital_formbuilder_form');
        }

        return $this->loadedData;
    }

    public function applyQuestions($model)
    {
        $this->questions->addFieldToFilter('form_id', $model->getId());
        $qs = $this->questions->getItems();
        $this->loadedData[$model->getId()]['questions'] =[];
        /**
         * @var $q QuestionsInterface
         */
        foreach ($qs as $q) {
            $q = $q->getDataModel();
            $this->loadedData[$model->getId()]['questions'][] = [
                'questions_id' => $q->getQuestionsId(),
                'record_id'=> $q->getSortOrder(),
                'question' => $q->getQuestion(),
                'answer_type'=>$q->getAnswerType(),
                'available_answers'=>$q->getAvailableAnswers(),
                'questioncode'=>$q->getQuestioncode(),
                'required' => $q->getRequired(),
                'default_answer' => $q->getDefaultAnswer()
            ];
        }
    }
}
