<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model\Data;

use BoxLeafDigital\FormBuilder\Api\Data\FormInterface;
use BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface;

class Questions extends \Magento\Framework\Api\AbstractExtensibleObject implements QuestionsInterface
{

    /**
     * Get questions_id
     * @return string|null
     */
    public function getQuestionsId()
    {
        return $this->_get(self::QUESTIONS_ID);
    }

    /**
     * Set questions_id
     * @param string $questionsId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setQuestionsId($questionsId)
    {
        return $this->setData(self::QUESTIONS_ID, $questionsId);
    }

    /**
     * Get form_id
     * @return string|null
     */
    public function getFormId()
    {
        return $this->_get(self::FORM_ID);
    }

    /**
     * Set form_id
     * @param string $formId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setFormId($formId)
    {
        return $this->setData(self::FORM_ID, $formId);
    }


    /**
     * Get question
     * @return string|null
     */
    public function getQuestion()
    {
        return $this->_get(self::QUESTION);
    }

    /**
     * Set question
     * @param string $question
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setQuestion($question)
    {
        return $this->setData(self::QUESTION, $question);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\QuestionsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\FormBuilder\Api\Data\QuestionsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get answer_type
     * @return string|null
     */
    public function getAnswerType()
    {
        return $this->_get(self::ANSWER_TYPE);
    }

    /**
     * Set answer_type
     * @param string $answerType
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setAnswerType($answerType)
    {
        return $this->setData(self::ANSWER_TYPE, $answerType);
    }

    /**
     * Get available_answers
     * @return string|null
     */
    public function getAvailableAnswers()
    {
        return $this->_get(self::AVAILABLE_ANSWERS);
    }

    /**
     * Set available_answers
     * @param string $availableAnswers
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setAvailableAnswers($availableAnswers)
    {
        return $this->setData(self::AVAILABLE_ANSWERS, $availableAnswers);
    }

    /**
     * Get default_answer
     * @return string|null
     */
    public function getDefaultAnswer()
    {
        return $this->_get(self::DEFAULT_ANSWER);
    }

    /**
     * Set default_answer
     * @param string $defaultAnswer
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setDefaultAnswer($defaultAnswer)
    {
        return $this->setData(self::DEFAULT_ANSWER, $defaultAnswer);
    }

    /**
     * Get description
     * @return string|null
     */
    public function getDescription()
    {
        return $this->_get(self::DESCRIPTION);
    }

    /**
     * Set description
     * @param string $description
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get sort_order
     * @return string|null
     */
    public function getSortOrder()
    {
        return $this->_get(self::SORT_ORDER);
    }

    /**
     * Set sort_order
     * @param string $sortOrder
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return Questions
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED);
    }

    /**
     * Set updated_at
     * @param $updatedAt
     * @return Questions
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED, $updatedAt);
    }

    /**
     * @inheritDoc
     */
    public function setQuestioncode($questionCode)
    {
        return $this->setData(self::QUESTION_CODE, $questionCode);
    }

    /**
     * @inheritDoc
     */
    public function getQuestioncode()
    {
        return $this->_get(self::QUESTION_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setRequired($required)
    {
        return $this->setData(self::REQUIRED, $required);
    }

    /**
     * @inheritDoc
     */
    public function getRequired()
    {
        return $this->_get(self::REQUIRED);
    }
}

