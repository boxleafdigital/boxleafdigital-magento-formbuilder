<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */

namespace BoxLeafDigital\FormBuilder\Model\Data;

use BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface;

/**
 * Class Answers
 * @package BoxLeafDigital\FormBuilder\Model\Data
 */
class Answers extends \Magento\Framework\Api\AbstractExtensibleObject implements AnswersInterface
{

    /**
     * @inheritDoc
     */
    public function getAnswersId()
    {
        return $this->_get(self::ANSWERS_ID);
    }

    /**
     * @inheritDoc
     */
    public function setAnswersId($answersId)
    {
        return $this->setData(self::ANSWERS_ID, $answersId);
    }

    /**
     * @inheritDoc
     */
    public function getQuestionId()
    {
        return $this->_get(self::QUESTION_ID);
    }

    /**
     * @inheritDoc
     */
    public function setQuestionId($questionId)
    {
        return $this->setData(self::QUESTION_ID, $questionId);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\FormBuilder\Api\Data\AnswersExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * @inheritDoc
     */
    public function getIsGuest()
    {
        return $this->_get(self::IS_GUEST);
    }

    /**
     * @inheritDoc
     */
    public function setIsGuest($isGuest)
    {
        return $this->setData(self::IS_GUEST, $isGuest);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @inheritDoc
     */
    public function getResultId()
    {
        return $this->_get(self::RESULT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setResultId($resultId)
    {
        return $this->setData(self::RESULT_ID, $resultId);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED);
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED, $updatedAt);
    }


    /**
     * @inheritDoc
     */
    public function setResultValue($resultValue)
    {
        return $this->setData(self::RESULT_VALUE, $resultValue);
    }

    /**
     * @inheritDoc
     */
    public function getResultValue()
    {
        return $this->_get(self::RESULT_VALUE);
    }

    /**
     * @inheritDoc
     */
    public function setQuoteItemId($quoteItemId)
    {
        return $this->setData(self::QUOTE_ITEM_ID, $quoteItemId);
    }

    /**
     * @inheritDoc
     */
    public function getQuoteItemId()
    {
        return $this->_get(self::QUOTE_ITEM_ID);
    }

    /**
     * @inheritDoc
     */
    public function setQuestionCode($questionCode)
    {
        return $this->setData(self::QUESTION_CODE, $questionCode);
    }

    /**
     * @inheritDoc
     */
    public function getQuestionCode()
    {
        return $this->_get(self::QUESTION_CODE);
    }
}

