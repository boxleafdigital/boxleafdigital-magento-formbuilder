<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model\Data;


class Form extends \Magento\Framework\Api\AbstractExtensibleObject implements \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
{

    /**
     * Get form_id
     * @return string|null
     */
    public function getFormId()
    {
        return $this->_get(self::FORM_ID);
    }

    /**
     * @param string $formId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setFormId($formId)
    {
        return $this->setData(self::FORM_ID, $formId);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::FORM_TITLE);
    }

    /**
     * @param string $formName
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setTitle($formName)
    {
        return $this->setData(self::FORM_TITLE, $formName);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getBackendTitle()
    {
        return $this->_get(self::FORM_BACKEND_NAME);
    }

    /**
     * @param string $backendTitle
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setBackendTitle($backendTitle)
    {
        return $this->setData(self::FORM_BACKEND_NAME, $backendTitle);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\FormExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\FormBuilder\Api\Data\FormExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get form_descriptin
     * @return string|null
     */
    public function getInfoText()
    {
        return $this->_get(self::FORM_DESCRIPTIN);
    }

    /**
     * @param string $formDescriptin
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setInfoText($formDescriptin)
    {
        return $this->setData(self::FORM_DESCRIPTIN, $formDescriptin);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED);
    }

    /**
     * @param string $createdAt
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED);
    }

    /**
     * @param string $updatedAt
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED, $updatedAt);
    }

    /**
     * @param $fields
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setFields($fields)
    {
        return $this->setData('fields', $fields);
    }

    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface[]
     */
    public function getFields()
    {
        return $this->_get('fields');
    }

    public function setQuestionnaireCode($questioncode)
    {
        return $this->setData(self::CODE, $questioncode);
    }


    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|mixed|null
     */
    public function getQuestionnaireCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * @param $status
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }


    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|mixed|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * @param $storeId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|Form
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface|mixed|null
     */
    public function getStoreId()
    {
        return $this->_get(self::STORE_ID);
    }
}
