<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model\ResourceModel\Form;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'form_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \BoxLeafDigital\FormBuilder\Model\Form::class,
            \BoxLeafDigital\FormBuilder\Model\ResourceModel\Form::class
        );
    }
}

