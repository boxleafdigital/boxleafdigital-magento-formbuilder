<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model;

use BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface;
use BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Questions extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'boxleafdigital_formbuilder_questions';
    protected $questionsDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param QuestionsInterfaceFactory $questionsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \BoxLeafDigital\FormBuilder\Model\ResourceModel\Questions $resource
     * @param \BoxLeafDigital\FormBuilder\Model\ResourceModel\Questions\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        QuestionsInterfaceFactory $questionsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Questions $resource,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Questions\Collection $resourceCollection,
        array $data = []
    ) {
        $this->questionsDataFactory = $questionsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve questions model with questions data
     * @return QuestionsInterface
     */
    public function getDataModel()
    {
        $questionsData = $this->getData();
        
        $questionsDataObject = $this->questionsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $questionsDataObject,
            $questionsData,
            QuestionsInterface::class
        );
        
        return $questionsDataObject;
    }
}

