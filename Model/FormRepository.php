<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model;

use BoxLeafDigital\FormBuilder\Api\Data\FormInterfaceFactory;
use BoxLeafDigital\FormBuilder\Api\Data\FormSearchResultsInterfaceFactory;
use BoxLeafDigital\FormBuilder\Api\FormRepositoryInterface;
use BoxLeafDigital\FormBuilder\Model\ResourceModel\Form as ResourceForm;
use BoxLeafDigital\FormBuilder\Model\ResourceModel\Form\CollectionFactory as FormCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class FormRepository implements FormRepositoryInterface
{
    protected $formFactory;

    protected $extensibleDataObjectConverter;
    protected $dataFormFactory;

    protected $dataObjectHelper;

    protected $resource;

    private $storeManager;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $formCollectionFactory;

    /**
     * @param ResourceForm $resource
     * @param FormFactory $formFactory
     * @param FormInterfaceFactory $dataFormFactory
     * @param FormCollectionFactory $formCollectionFactory
     * @param FormSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceForm $resource,
        FormFactory $formFactory,
        FormInterfaceFactory $dataFormFactory,
        FormCollectionFactory $formCollectionFactory,
        FormSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->formFactory = $formFactory;
        $this->formCollectionFactory = $formCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataFormFactory = $dataFormFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \BoxLeafDigital\FormBuilder\Api\Data\FormInterface $form
    ) {
        $formData = $this->extensibleDataObjectConverter->toNestedArray(
            $form,
            [],
            \BoxLeafDigital\FormBuilder\Api\Data\FormInterface::class
        );

        $formModel = $this->formFactory->create()->setData($formData);

        try {
            $this->resource->save($formModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the form: %1',
                $exception->getMessage()
            ));
        }
        return $formModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($formId)
    {
        $form = $this->formFactory->create();
        $this->resource->load($form, $formId);
        if (!$form->getId()) {
            throw new NoSuchEntityException(__('Form with id "%1" does not exist.', $formId));
        }
        return $form->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getByCode($code)
    {
        $form = $this->formFactory->create();
        $this->resource->load($form, $code, 'questionnaire_code');
        if (!$form->getId()) {
            throw new NoSuchEntityException(__('Form with form code "%1" does not exist.', $code));
        }
        return $form->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->formCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \BoxLeafDigital\FormBuilder\Api\Data\FormInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \BoxLeafDigital\FormBuilder\Api\Data\FormInterface $form
    ) {
        try {
            $formModel = $this->formFactory->create();
            $this->resource->load($formModel, $form->getFormId());
            $this->resource->delete($formModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Form: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($formId)
    {
        return $this->delete($this->get($formId));
    }
}
