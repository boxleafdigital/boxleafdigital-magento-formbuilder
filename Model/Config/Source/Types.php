<?php
namespace BoxLeafDigital\FormBuilder\Model\Config\Source;

use Magento\Customer\Model\Data\Customer;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;

class Types implements \Magento\Framework\Data\OptionSourceInterface
{


    /**
     * Retrieve Custom Option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options=[];
        $types = ['chck','radio','slct','inpt','textarea','password','email'];
        foreach ($types as $type) {
            $options[] = ['value'=>$type, 'label' => __(ucwords($type))];
        }
        return $options;
    }
}
