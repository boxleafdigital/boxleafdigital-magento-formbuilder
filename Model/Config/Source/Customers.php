<?php
namespace BoxLeafDigital\FormBuilder\Model\Config\Source;

use Magento\Customer\Model\Data\Customer;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;

class Customers implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var CollectionFactory
     */
    private $_customCollection;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->_customCollection = $collectionFactory;
    }

    /**
     * Retrieve Custom Option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $customers = $this->_customCollection->create();
        $options = [];
        /**
         * @var $customer Customer
         */
        foreach ($customers as $customer) {
            $options[] = ['value'=>$customer->getId(), 'label' => $customer->getEmail()];
        }
        return $options;
    }
}
