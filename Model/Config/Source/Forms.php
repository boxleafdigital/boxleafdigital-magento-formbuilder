<?php
namespace BoxLeafDigital\FormBuilder\Model\Config\Source;

use BoxLeafDigital\FormBuilder\Model\Data\Form;
use BoxLeafDigital\FormBuilder\Model\ResourceModel\Form\CollectionFactory;

class Forms   extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var CollectionFactory
     */
    private $_customCollection;

    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->_customCollection = $collectionFactory;
    }

    /**
     * Retrieve Custom Option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $forms = $this->_customCollection->create();
        $options = [];
        /**
         * @var $form Form
         */
        $options[] =[
            'value' => '',
            'label' => __('-- Please Select --')
        ];
        foreach ($forms as $form) {
            $options[] = ['value'=>$form->getFormId(), 'label' => $form->getTitle()];
        }
        return $options;
    }

    /**
     * Retrieve All options
     *
     * @return array
     */
    public function getAllOptions()
    {
        return $this->toOptionArray();
    }
}
