<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model;

use BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface;
use BoxLeafDigital\FormBuilder\Api\Data\AnswersInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Answers extends \Magento\Framework\Model\AbstractModel
{
    protected $answersDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'boxleafdigital_formbuilder_answers';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param AnswersInterfaceFactory $answersDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \BoxLeafDigital\FormBuilder\Model\ResourceModel\Answers $resource
     * @param \BoxLeafDigital\FormBuilder\Model\ResourceModel\Answers\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        AnswersInterfaceFactory $answersDataFactory,
        DataObjectHelper $dataObjectHelper,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Answers $resource,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Answers\Collection $resourceCollection,
        array $data = []
    ) {
        $this->answersDataFactory = $answersDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve answers model with answers data
     * @return AnswersInterface
     */
    public function getDataModel()
    {
        $answersData = $this->getData();

        $answersDataObject = $this->answersDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $answersDataObject,
            $answersData,
            AnswersInterface::class
        );

        return $answersDataObject;
    }
}
