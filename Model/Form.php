<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Model;

use BoxLeafDigital\FormBuilder\Api\Data\FormInterface;
use BoxLeafDigital\FormBuilder\Api\Data\FormInterfaceFactory;
use BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface;
use BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Form extends \Magento\Framework\Model\AbstractModel
{
    protected $dataObjectHelper;

    protected $formDataFactory;

    protected $_eventPrefix = 'boxleafdigital_formbuilder_form';
    /**
     * @var ResourceModel\Questions\Collection
     */
    private $questionsCollection;
    /**
     * @var QuestionsInterfaceFactory
     */
    private $questionsDataFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param FormInterfaceFactory $formDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \BoxLeafDigital\FormBuilder\Model\ResourceModel\Form $resource
     * @param \BoxLeafDigital\FormBuilder\Model\ResourceModel\Form\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        FormInterfaceFactory $formDataFactory,
        QuestionsInterfaceFactory $questionsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Form $resource,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Questions\CollectionFactory $questionsCollection,
        \BoxLeafDigital\FormBuilder\Model\ResourceModel\Form\Collection $resourceCollection,
        array $data = []
    ) {
        $this->formDataFactory = $formDataFactory;
        $this->questionsDataFactory = $questionsDataFactory;
        $this->questionsCollection = $questionsCollection->create();
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve form model with form data
     * @return FormInterface
     */
    public function getDataModel()
    {
        $formData = $this->getData();

        $formDataObject = $this->formDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $formDataObject,
            $formData,
            FormInterface::class
        );

        $fields = [];
        $qs = $this->getQuestions();
        foreach ($qs as $q) {
            $qDataObject = $this->questionsDataFactory->create();
            $this->dataObjectHelper->populateWithArray(
               $qDataObject,
               $q->getData(),
               QuestionsInterface::class
           );
            $fields[] = $qDataObject;
        }

        $formDataObject->setFields($fields);

        return $formDataObject;
    }

    public function getBackUrl() {

    }

    /**
     * @return \Magento\Framework\DataObject[]
     */
    public function getQuestions()
    {
        $this->questionsCollection->addFieldToFilter('form_id', $this->getData('form_id'));
        return $this->questionsCollection->getItems();
    }
}
