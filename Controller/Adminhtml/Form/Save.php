<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Controller\Adminhtml\Form;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('form_id');

            $model = $this->_objectManager->create(\BoxLeafDigital\FormBuilder\Model\Form::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Form no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $questions = [];
            if (isset($data['questions'])) {
                $questions = $data['questions'];

                unset($data['questions']);
            }

            $model->setData($data);

            try {
                $model->save();

                $originalIds = [];
                $newIds = [];
                //get origginal connections
                foreach ($model->getQuestions() as $q) {
                    $originalIds[] = $q->getData('questions_id');
                }

                //save the linked questions
                foreach ($questions as $question) {
                    $qid = $question['questions_id'] != '' ? $question['questions_id'] : null;
                    if ($qid == null) {
                        unset($question['questions_id']);
                    }

                    $question['sort_order'] = (int)$question['record_id'];
                    unset($question['record_id']);


                    $question['form_id'] = $model->getId();

                    $qmodel = $this->_objectManager->create(\BoxLeafDigital\FormBuilder\Model\Questions::class)->load($qid);

                    $qmodel->setData($question);
                    try {
                        $qmodel->save();
                        $newIds[] = $qmodel->getData('questions_id');
                    } catch (LocalizedException $e) {
                        $this->messageManager->addErrorMessage($e->getMessage());
                    } catch (\Exception $e) {
                        $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving a Question.'));
                    }
                }

                // delete the old
                $delete = array_diff($originalIds, $newIds);
                foreach ($delete as $id) {
                    $qmodel = $this->_objectManager->create(\BoxLeafDigital\FormBuilder\Model\Questions::class)->load($id);
                    $qmodel->delete();
                }

                $this->messageManager->addSuccessMessage(__('You saved the Form.'));
                $this->dataPersistor->clear('boxleafdigital_formbuilder_form');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['form_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Form.'));
            }

            $this->dataPersistor->set('boxleafdigital_formbuilder_form', $data);
            return $resultRedirect->setPath('*/*/edit', ['form_id' => $this->getRequest()->getParam('form_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
