<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Controller\Form;

use BoxLeafDigital\FormBuilder\Api\Data\AnswersInterfaceFactory;
use BoxLeafDigital\FormBuilder\Model\AnswersRepository;
use BoxLeafDigital\FormBuilder\Service\AddAnswer;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Phrase;

/**
 * Post create customer action
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SubmitPost extends Action implements CsrfAwareActionInterface, HttpPostActionInterface
{

    /**
     * @var RedirectInterface
     */
    private $redirect;

    private $formKeyValidator;
    /**
     * @var AnswersInterfaceFactory
     */
    private $answersInterfaceFactory;
    /**
     * @var AnswersRepository
     */
    private $answersRepository;
    /**
     * @var Session
     */
    private $customerSession;
    /**
     * @var AddAnswer
     */
    private $addAnswer;

    /**
     * SubmitPost constructor.
     * @param Context $context
     * @param AnswersInterfaceFactory $answersInterfaceFactory
     * @param AnswersRepository $answersRepository
     * @param RedirectInterface $redirect
     * @param Session $customerSession
     * @param Validator|null $formKeyValidator
     */
    public function __construct(
        Context $context,
        AnswersInterfaceFactory $answersInterfaceFactory,
        AnswersRepository $answersRepository,
        RedirectInterface $redirect,
        Session $customerSession,
        AddAnswer $addAnswer,
        Validator $formKeyValidator = null
    ) {
        parent::__construct($context);
        $this->redirect =$redirect;
        $this->answersInterfaceFactory =$answersInterfaceFactory;
        $this->answersRepository =$answersRepository;
        $this->customerSession =$customerSession;
        $this->addAnswer =$addAnswer;
        $this->formKeyValidator = $formKeyValidator ?: ObjectManager::getInstance()->get(Validator::class);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $url =  $this->redirect->getRefererUrl();
        if (!$this->getRequest()->isPost()
            || !$this->formKeyValidator->validate($this->getRequest())
        ) {
            return $this->resultRedirectFactory->create()
                ->setUrl($this->_redirect->error($url));
        }

        $data = $this->getRequest()->getParams();
        $this->addAnswer->execute($data, true);

        return $this->resultRedirectFactory->create()
            ->setUrl($this->_redirect->success($url));
    }

    /**
     * Create exception in case CSRF validation failed.
     * Return null if default exception will suffice.
     *
     * @param RequestInterface $request
     *
     * @return InvalidRequestException|null
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $url =  $this->redirect->getRefererUrl();
        $resultRedirect->setUrl($this->_redirect->error($url));

        return new InvalidRequestException(
            $resultRedirect,
            [new Phrase('Invalid Form Key. Please refresh the page.')]
        );
    }

    /**
     * Perform custom request validation.
     * Return null if default validation is needed.
     *
     * @param RequestInterface $request
     *
     * @return bool|null
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return null;
    }
}
