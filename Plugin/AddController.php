<?php
namespace BoxLeafDigital\FormBuilder\Plugin;

use BoxLeafDigital\FormBuilder\Model\QuestionsRepository;
use BoxLeafDigital\FormBuilder\Service\AddAnswer;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class AddController
{
    protected $request;
    /**
     * @var AddAnswer
     */
    private $addAnswer;
    /**
     * @var ManagerInterface
     */
    private $message;
    /**
     * @var Session
     */
    private $checkoutSession;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var QuestionsRepository
     */
    private $questionsRepository;

    /**
     * Plugin constructor.
     *
     * @param Http $request
     * @param Session $checkoutSession
     * @param ProductRepositoryInterface $productRepository
     * @param ManagerInterface $message
     * @param StoreManagerInterface $storeManager
     * @param AddAnswer $addAnswer
     */
    public function __construct(
        Http $request,
        Session $checkoutSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Message\ManagerInterface $message,
        StoreManagerInterface $storeManager,
        QuestionsRepository $questionsRepository,
        AddAnswer $addAnswer
    ) {
        $this->request = $request;
        $this->addAnswer = $addAnswer;
        $this->message = $message;
        $this->productRepository = $productRepository;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->questionsRepository = $questionsRepository;
    }

    protected function _getProduct()
    {
        $productId = (int)$this->request->getParam('product');
        if ($productId) {
            $storeId = $this->storeManager->getStore()->getId();
            try {
                return $this->productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }

    public function beforeExecute(
        \Magento\Checkout\Controller\Cart\Add $subject
    ) {
        try {
            $product = $this->_getProduct();
        } catch (LocalizedException $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }
        $questionError = false;
        $data = $this->request->getParams();
        if (!isset($data['question'])) {
            if ($product->getForm() || $product->getData('pquestionnaire')) {
                if (is_array($data["question"])) {
                    if (sizeof($data["question"]) < 1) {
                        $questionError = true;
                    }
                } else {
                    $questionError = true;
                }
            }
        }

        if ($questionError) {
            throw new LocalizedException(__('Form is required, please fill in the form'));
        }

        return [ ];
    }

    /**
     * @param \Magento\Checkout\Model\Cart $subject
     * @param \Closure $proceed
     * @param $productInfo
     * @param $requestInfo
     * @return mixed
     * @throws LocalizedException
     */
    public function afterExecute(
        \Magento\Checkout\Controller\Cart\Add $subject,
        $result
    ) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');

        // get quote items collection
        $itemsCollection = $cart->getQuote()->getItemsCollection();
        $lastItem = $itemsCollection->getLastItem();
        $quoteItemId = $lastItem->getId();

        //get the data from the add to cart reequest
        $data = $this->request->getParams();
        if (isset($data['question'])) {
            //execute the result to the service contract to save the data
            $r = $this->addAnswer->execute($data, false, $quoteItemId);
            if ($r == false) {
                throw new LocalizedException(__('Form is required, please fill in the form'));
            }

            $additionalOptions = [];
            if ($additionalOption = $lastItem->getOptionByCode('additional_options')) {
                $additionalOptions = json_decode($additionalOption->getValue(), true);
            }

            foreach ($data['question'] as $id => $value) {
                try {
                    $question = $this->questionsRepository->get($id);
                    $additionalOptions[] = [
                        'label' => $question->getQuestion(),
                        'value' => $value
                    ];
                } catch (\Exception $e) {
                }
            }

            if (count($additionalOptions) > 0) {
                $lastItem->addOption([
                    'product_id' => $lastItem->getProduct()->getProductId(),
                    'code' => 'additional_options',
                    'value' => json_encode($additionalOptions)
                ]);
            }

            $lastItem->setData('orderanswers', json_encode($r));
            $lastItem->save();
        }



        return $result;
    }
}
