<?php
namespace BoxLeafDigital\FormBuilder\Plugin\Quote;

use BoxLeafDigital\FormBuilder\Api\AnswersRepositoryInterface;
use BoxLeafDigital\FormBuilder\Model\QuestionsRepository;
use Exception;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Message\ManagerInterface;

class Item
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var ManagerInterface
     */
    private $message;

    private $searchCriteriaBuilder;
    /**
     * @var AnswersRepositoryInterface
     */
    private $answersRepository;
    /**
     * @var QuestionsRepository
     */
    private $questionsRepository;

    /**
     * Plugin constructor.
     *
     * @param \Magento\Framework\App\Request\Http $request
     * @param ManagerInterface $message
     * @param SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
     * @param AnswersRepositoryInterface $answersRepository
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Message\ManagerInterface $message,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        QuestionsRepository $questionsRepository,
        AnswersRepositoryInterface $answersRepository
    ) {
        $this->request = $request;
        $this->searchCriteriaBuilder = $searchCriteriaBuilderFactory;
        $this->answersRepository = $answersRepository;
        $this->questionsRepository = $questionsRepository;
        $this->message = $message;
    }

    public function aroundRepresentProduct(\Magento\Quote\Model\Quote\Item $subject, \Closure $proceed, $product)
    {
        try {
            $represent = $proceed($product);
        } catch (Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }

        $data = $this->request->getParams();
        if (isset($data['question'])) {
            $qs = count($data['question']);
            $same = 0;

            foreach ($data['question'] as $id => $value) {
                $filters = $this->searchCriteriaBuilder->create();
                $filters->addFilter('quote_item_id', $subject->getId());
                $filters->addFilter('result_value', $value);
                $filters->addFilter('question_Id', $id);
                $results = $this->answersRepository->getList($filters->create());
                if ($results->getTotalCount() > 0) {
                    $same++;
                }
            }

            if ($qs != $same) {
                $represent = false;
            }
        }
        return $represent;
    }
}
