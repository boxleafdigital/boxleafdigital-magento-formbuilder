<?php
namespace BoxLeafDigital\FormBuilder\Plugin;

use BoxLeafDigital\FormBuilder\Model\QuestionsRepository;
use BoxLeafDigital\FormBuilder\Service\AddAnswer;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class CartController
{
    protected $request;
    /**
     * @var AddAnswer
     */
    private $addAnswer;
    /**
     * @var ManagerInterface
     */
    private $message;
    /**
     * @var Session
     */
    private $checkoutSession;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var QuestionsRepository
     */
    private $questionsRepository;

    /**
     * Plugin constructor.
     *
     * @param Http $request
     * @param Session $checkoutSession
     * @param ProductRepositoryInterface $productRepository
     * @param ManagerInterface $message
     * @param StoreManagerInterface $storeManager
     * @param AddAnswer $addAnswer
     */
    public function __construct(
        Http $request,
        Session $checkoutSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Message\ManagerInterface $message,
        StoreManagerInterface $storeManager,
        QuestionsRepository $questionsRepository,
        AddAnswer $addAnswer
    ) {
        $this->request = $request;
        $this->addAnswer = $addAnswer;
        $this->message = $message;
        $this->productRepository = $productRepository;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->questionsRepository = $questionsRepository;
    }

    protected function _getItem()
    {
        $id = (int)$this->request->getParam('id');
        $objectManager = ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');

        // get quote items collection
        $quoteItem = $cart->getQuote()->getItemById($id);

        if ($quoteItem) {
            return $quoteItem;
        }

        throw new LocalizedException(__('item doesnt exist'));
    }

    public function beforeExecute(
        \Magento\Checkout\Controller\Cart\UpdateItemOptions $subject
    ) {
        try {
            $item = $this->_getItem();
            $product = $item->getProduct();
        } catch (LocalizedException $e) {
            throw new LocalizedException(
                __($e->getMessage())
            );
        }
        $questionError = false;
        $data = $this->request->getParams();
        if (!isset($data['question'])) {
            if ($product->getForm() || $product->getData('pquestionnaire')) {
                if (is_array($data["question"])) {
                    if (sizeof($data["question"]) < 1) {
                        $questionError = true;
                    }
                } else {
                    $questionError = true;
                }
            }
        }

        if ($questionError) {
            throw new LocalizedException(__('Form is required, please fill in the form'));
        }

        return [ ];
    }

    /**
     * @param \Magento\Checkout\Controller\Cart\UpdateItemOptions $subject
     * @param $result
     * @return mixed
     * @throws LocalizedException
     */
    public function afterExecute(
        \Magento\Checkout\Controller\Cart\UpdateItemOptions $subject,
        $result
    ) {
        try {
            $item = $this->_getItem();
        } catch (\Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
        $quoteItemId = $item->getId();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');

        // get quote items collection
        $itemsCollection = $cart->getQuote()->getItemsCollection();
        $lastItem = $itemsCollection->getLastItem();
        $newQuoteId = $lastItem->getId();

        //get the data from the add to cart reequest
        $data = $this->request->getParams();
        if (isset($data['question'])) {
            //execute the result to the service contract to save the data
            $r = $this->addAnswer->execute($data, false, $quoteItemId, $newQuoteId);
            if ($r == false) {
                throw new LocalizedException(__('Form is required, please fill in the form'));
            }

            $additionalOptions = [];

            foreach ($data['question'] as $id => $value) {
                try {
                    $question = $this->questionsRepository->get($id);
                    $additionalOptions[] = [
                        'label' => $question->getQuestion(),
                        'value' => $value
                    ];
                } catch (\Exception $e) {
                }
            }

            if (count($additionalOptions) > 0) {
                $lastItem->addOption([
                    'product_id' => $lastItem->getProduct()->getProductId(),
                    'code' => 'additional_options',
                    'value' => json_encode($additionalOptions)
                ]);
            }

            $lastItem->setData('orderanswers', json_encode($r));
            $lastItem->save();
        }

        return $result;
    }
}
