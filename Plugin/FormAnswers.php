<?php
namespace BoxLeafDigital\FormBuilder\Plugin;


class FormAnswers
{
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setData('orderanswers', $item->getData('orderanswers'));
        return $orderItem;
    }
}
