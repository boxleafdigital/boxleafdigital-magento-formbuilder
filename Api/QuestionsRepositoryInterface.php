<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface QuestionsRepositoryInterface
{

    /**
     * Save Questions
     * @param \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface $questions
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface $questions
    );

    /**
     * Retrieve Questions
     * @param string $questionsId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($questionsId);

    /**
     * Retrieve Questions matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Questions
     * @param \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface $questions
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface $questions
    );

    /**
     * Delete Questions by ID
     * @param string $questionsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($questionsId);
}

