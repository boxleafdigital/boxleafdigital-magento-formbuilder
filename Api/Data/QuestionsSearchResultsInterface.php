<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api\Data;

interface QuestionsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Questions list.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface[]
     */
    public function getItems();

    /**
     * Set question list.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

