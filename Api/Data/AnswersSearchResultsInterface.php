<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api\Data;

interface AnswersSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Answers list.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface[]
     */
    public function getItems();

    /**
     * Set question_id list.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

