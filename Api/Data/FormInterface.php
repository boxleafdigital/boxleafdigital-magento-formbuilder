<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api\Data;

interface FormInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const FORM_TITLE = 'title';
    const FORM_BACKEND_NAME = 'backend_title';
    const STORE_ID = 'store_id';
    const FORM_DESCRIPTIN = 'info_text';
    const FORM_ID = 'form_id';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';
    const STATUS = 'status';
    const CODE = 'questionnaire_code';

    /**
     * Get form_id
     * @return string|null
     */
    public function getFormId();

    /**
     * Set form_id
     * @param string $formId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setFormId($formId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $formName
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setTitle($formName);

    /**
     * Get title
     * @return string|null
     */
    public function getBackendTitle();

    /**
     * Set backend_title
     * @param string $backendTitle
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setBackendTitle($backendTitle);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\FormExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\FormBuilder\Api\Data\FormExtensionInterface $extensionAttributes
    );

    /**
     * Get form_descriptin
     * @return string|null
     */
    public function getInfoText();

    /**
     * Set form_descriptin
     * @param string $formDescriptin
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setInfoText($formDescriptin);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @param $fields
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setFields($fields);

    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsInterface[]
     */
    public function getFields();

    /**
     * @param $questionaireCode
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setQuestionnaireCode($questionaireCode);

    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function getQuestionnaireCode();

    /**
     * @param $status
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setStatus($status);

    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function getStatus();

    /**
     * @param $status
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function setStoreId($storeId);

    /**
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     */
    public function getStoreId();
}

