<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api\Data;

interface QuestionsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const SORT_ORDER = 'sort_order';
    const QUESTIONS_ID = 'questions_id';
    const FORM_ID = 'form_id';
    const AVAILABLE_ANSWERS = 'available_answers';
    const DEFAULT_ANSWER = 'default_answer';
    const DESCRIPTION = 'description';
    const QUESTION = 'question';
    const ANSWER_TYPE = 'answer_type';
    const QUESTION_CODE = 'questioncode';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';
    const REQUIRED = 'required';

    /**
     * Get questions_id
     * @return string|null
     */
    public function getQuestionsId();

    /**
     * Set questions_id
     * @param string $questionsId
     * @return QuestionsInterface
     */
    public function setQuestionsId($questionsId);

    /**
     * Get form_id
     * @return string|null
     */
    public function getFormId();

    /**
     * Set form_id
     * @param string $formId
     * @return QuestionsInterface
     */
    public function setFormId($formId);

    /**
     * Get question
     * @return string|null
     */
    public function getQuestion();

    /**
     * Set question
     * @param string $question
     * @return QuestionsInterface
     */
    public function setQuestion($question);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\QuestionsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\QuestionsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\FormBuilder\Api\Data\QuestionsExtensionInterface $extensionAttributes
    );

    /**
     * Get answer_type
     * @return string|null
     */
    public function getAnswerType();

    /**
     * Set answer_type
     * @param string $answerType
     * @return QuestionsInterface
     */
    public function setAnswerType($answerType);

    /**
     * Get available_answers
     * @return string|null
     */
    public function getAvailableAnswers();

    /**
     * Set available_answers
     * @param string $availableAnswers
     * @return QuestionsInterface
     */
    public function setAvailableAnswers($availableAnswers);

    /**
     * Get default_answer
     * @return string|null
     */
    public function getDefaultAnswer();

    /**
     * Set default_answer
     * @param string $defaultAnswer
     * @return QuestionsInterface
     */
    public function setDefaultAnswer($defaultAnswer);

    /**
     * Get description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set description
     * @param string $description
     * @return QuestionsInterface
     */
    public function setDescription($description);

    /**
     * Get sort_order
     * @return string|null
     */
    public function getSortOrder();

    /**
     * Set sort_order
     * @param string $sortOrder
     * @return QuestionsInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return QuestionsInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return QuestionsInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @param string $questionCode
     * @return QuestionsInterface
     */
    public function setQuestioncode($questionCode);

    /**
     * @return string|null
     */
    public function getQuestioncode();

    /**
     * @param string $required
     * @return QuestionsInterface
     */
    public function setRequired($required);

    /**
     * @return string|null
     */
    public function getRequired();
}

