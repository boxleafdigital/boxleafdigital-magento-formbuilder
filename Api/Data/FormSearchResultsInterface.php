<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api\Data;

interface FormSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Form list.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\FormInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

