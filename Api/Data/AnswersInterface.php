<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api\Data;

interface AnswersInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const QUESTION_ID = 'question_id';
    const ANSWERS_ID = 'answers_id';
    const IS_GUEST = 'is_guest';
    const RESULT_VALUE = 'result_value';
    const QUESTION_CODE = 'questioncode';
    const CUSTOMER_ID = 'customer_id';
    const RESULT_ID = 'result_id';
    const CREATED = 'created_at';
    const UPDATED = 'updated_at';
    const QUOTE_ITEM_ID = 'quote_item_id';
    /**
     * Get answers_id
     * @return int|null
     */
    public function getAnswersId();

    /**
     * Set answers_id
     * @param int $answersId
     * @return AnswersInterface
     */
    public function setAnswersId($answersId);

    /**
     * Get question_id
     * @return int|null
     */
    public function getQuestionId();

    /**
     * Set question_id
     * @param int $questionId
     * @return AnswersInterface
     */
    public function setQuestionId($questionId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \BoxLeafDigital\FormBuilder\Api\Data\AnswersExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \BoxLeafDigital\FormBuilder\Api\Data\AnswersExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \BoxLeafDigital\FormBuilder\Api\Data\AnswersExtensionInterface $extensionAttributes
    );

    /**
     * Get is_guest
     * @return boolean|null
     */
    public function getIsGuest();

    /**
     * Set is_guest
     * @param boolean $isGuest
     * @return AnswersInterface
     */
    public function setIsGuest($isGuest);

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return AnswersInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get result_id
     * @return string|null
     */
    public function getResultId();

    /**
     * Set result_id
     * @param string $resultId
     * @return AnswersInterface
     */
    public function setResultId($resultId);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return AnswersInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return AnswersInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @param string $resultValue
     * @return AnswersInterface
     */
    public function setResultValue($resultValue);

    /**
     * @return string|null
     */
    public function getResultValue();

    /**
     * @param string $quoteItemId
     * @return AnswersInterface
     */
    public function setQuoteItemId($quoteItemId);

    /**
     * @return string|null
     */
    public function getQuoteItemId();

    /**
     * @param string $questionCode
     * @return AnswersInterface
     */
    public function setQuestionCode($questionCode);

    /**
     * @return string|null
     */
    public function getQuestionCode();

}
