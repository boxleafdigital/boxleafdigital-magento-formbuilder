<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface AnswersRepositoryInterface
{

    /**
     * Save Answers
     * @param \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface $answers
     * @return \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface $answers
    );

    /**
     * Retrieve Answers
     * @param string $answersId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($answersId);

    /**
     * Retrieve Answers matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \BoxLeafDigital\FormBuilder\Api\Data\AnswersSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Answers
     * @param \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface $answers
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \BoxLeafDigital\FormBuilder\Api\Data\AnswersInterface $answers
    );

    /**
     * Delete Answers by ID
     * @param string $answersId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($answersId);
}

