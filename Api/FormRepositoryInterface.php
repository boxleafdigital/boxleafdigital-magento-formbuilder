<?php
/**
 * Copyright © BoxLeaf Digital 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace BoxLeafDigital\FormBuilder\Api;

interface FormRepositoryInterface
{

    /**
     * Save Form
     * @param \BoxLeafDigital\FormBuilder\Api\Data\FormInterface $form
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \BoxLeafDigital\FormBuilder\Api\Data\FormInterface $form
    );

    /**
     * Retrieve Form
     * @param string $formId
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($formId);

    /**
     * Retrieve Form
     * @param string $code
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByCode($code);

    /**
     * Retrieve Form matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \BoxLeafDigital\FormBuilder\Api\Data\FormSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Form
     * @param \BoxLeafDigital\FormBuilder\Api\Data\FormInterface $form
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \BoxLeafDigital\FormBuilder\Api\Data\FormInterface $form
    );

    /**
     * Delete Form by ID
     * @param string $formId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($formId);
}
